package me.jvt.contentnegotiation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import me.jvt.http.mediatype.MediaType;

public class ContentTypeNegotiator {

  private static final List<MediaType> DEFAULT_MEDIA_TYPES =
      Collections.singletonList(MediaType.WILDCARD);

  private final List<MediaType> supportedMediaTypes;

  public ContentTypeNegotiator() {
    this(DEFAULT_MEDIA_TYPES);
  }

  public ContentTypeNegotiator(List<MediaType> supportedMediaTypes) {
    if (supportedMediaTypes.isEmpty()) {
      throw new IllegalArgumentException(
          "Unable to construct a ContentTypeNegotiator without supported MediaTypes. If you want to allow any MediaType, you need to use the default constructor");
    }
    this.supportedMediaTypes = Collections.unmodifiableList(sort(supportedMediaTypes));
  }

  /**
   * From a set of {@link MediaType}s that the client is requesting, and a pre-configured set of
   * {@link MediaType}s that the server supports, produce a {@link MediaType} that is allowed.
   *
   * @param requested the {@link MediaType}s that the client supports
   * @return {@link MediaType} that is most suitable
   * @throws NotAcceptableException if the {@link MediaType}s the client requested are not supported
   */
  public MediaType negotiate(List<MediaType> requested) throws NotAcceptableException {
    if (null == requested || requested.isEmpty()) {
      throw NotAcceptableException.noMatches();
    }

    List<MediaType> sorted = sort(requested);
    List<MediaType> supportedAndCompatible =
        sorted.stream()
            .filter(
                mediaType ->
                    supportedMediaTypes.stream().anyMatch(isMediaTypeCompatible(mediaType)))
            .collect(Collectors.toList());

    if (supportedAndCompatible.isEmpty()) {
      throw NotAcceptableException.noMatches();
    }

    MediaType found = mapToMatchedMediaType(supportedAndCompatible.get(0));

    return withoutQuality(found);
  }

  private List<MediaType> sort(List<MediaType> mediaTypes) {
    List<MediaType> sorted = new ArrayList<>(mediaTypes);
    sorted.sort(MediaType.COMPARATOR);
    return sorted;
  }

  private MediaType mapToMatchedMediaType(MediaType mediaType) throws NotAcceptableException {
    if (supportedMediaTypes.contains(MediaType.WILDCARD)) {
      return MediaType.WILDCARD;
    }

    List<MediaType> found =
        supportedMediaTypes.stream()
            .filter(isMediaTypeCompatible(mediaType))
            .collect(Collectors.toList());
    assertOnlyOneMatch(found);

    return found.get(0);
  }

  private Predicate<MediaType> isMediaTypeCompatible(MediaType mediaType) {
    return supported -> supported.isCompatible(mediaType);
  }

  private void assertOnlyOneMatch(List<MediaType> mediaTypes) throws NotAcceptableException {
    if (mediaTypes.isEmpty()) {
      throw NotAcceptableException.noMatches();
    }
    if (mediaTypes.size() > 1) {
      throw NotAcceptableException.multipleMatches();
    }
  }

  private MediaType withoutQuality(MediaType from) {
    Map<String, String> params = new HashMap<>(from.getParameters());
    params.remove("q");
    return new MediaType(from.getType(), from.getSubtype(), params);
  }
}
