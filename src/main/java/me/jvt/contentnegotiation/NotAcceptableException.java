package me.jvt.contentnegotiation;

/**
 * Exception to capture the fact that the Content Negotiation has failed, and should be mapped to an
 * <a href="https://http.cat/406">HTTP 406 Not Acceptable</a> status code.
 */
public class NotAcceptableException extends Exception {

  NotAcceptableException(String message) {
    super(message);
  }

  public static NotAcceptableException noMatches() {
    return new NotAcceptableException(
        "The requested media types cannot be supported by this server");
  }

  public static NotAcceptableException multipleMatches() {
    return new NotAcceptableException(
        "The requested media types could not be determined, as multiple matches were found");
  }
}
