package me.jvt.contentnegotiation.support;

import java.util.Comparator;

public class WhitespaceIgnoringComparator implements Comparator<String> {
  @Override
  public int compare(String s1, String s2) {
    return s1.replaceAll("\\s+", "").compareTo(s2.replaceAll("\\s+", ""));
  }
}
