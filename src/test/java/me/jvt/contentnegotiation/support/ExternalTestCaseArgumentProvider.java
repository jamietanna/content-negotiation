package me.jvt.contentnegotiation.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

public class ExternalTestCaseArgumentProvider implements ArgumentsProvider {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public ExternalTestCaseArgumentProvider() {}

  @Override
  public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
    ObjectMapper objectMapper = new ObjectMapper();
    ExternalTestCase cases =
        objectMapper.readValue(
            getClass()
                .getClassLoader()
                .getResourceAsStream("content-negotiation-test-cases/cases.json"),
            ExternalTestCase.class);

    return cases.cases.stream()
        .map(c -> Arguments.of(c.name, c.supported, c.acceptHeader, c.negotiated));
  }

  public static class ExternalTestCase {
    List<Case> cases;

    public List<Case> getCases() {
      return cases;
    }

    public void setCases(List<Case> cases) {
      this.cases = cases;
    }

    public static class Case {
      String name;
      List<String> supported;
      String acceptHeader;
      List<String> negotiated;

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public List<String> getSupported() {
        return supported;
      }

      public void setSupported(List<String> supported) {
        this.supported = supported;
      }

      public String getAcceptHeader() {
        return acceptHeader;
      }

      public void setAcceptHeader(String acceptHeader) {
        this.acceptHeader = acceptHeader;
      }

      public List<String> getNegotiated() {
        return negotiated;
      }

      public void setNegotiated(List<String> negotiated) {
        this.negotiated = negotiated;
      }
    }
  }
}
