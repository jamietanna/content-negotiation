package me.jvt.contentnegotiation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import me.jvt.contentnegotiation.support.ExternalTestCaseArgumentProvider;
import me.jvt.contentnegotiation.support.WhitespaceIgnoringComparator;
import me.jvt.http.mediatype.AcceptHeaderParser;
import me.jvt.http.mediatype.MediaType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

class ContentTypeNegotiatorTest {

  private final MediaType JSON_API = MediaType.valueOf("application/vnd.api+json");
  private ContentTypeNegotiator negotiator;

  @BeforeEach
  void setup() {
    negotiator = new ContentTypeNegotiator(asList(MediaType.APPLICATION_JSON));
  }

  @Nested
  class DefaultConstructor {

    @Test
    void canBeUsed() {
      assertThatCode(ContentTypeNegotiator::new).doesNotThrowAnyException();
    }

    @ParameterizedTest
    @ValueSource(strings = {"application/json", "text/html"})
    void allowsAnything(String stringValue) {
      MediaType mediaType = MediaType.valueOf(stringValue);

      ContentTypeNegotiator negotiator = new ContentTypeNegotiator();

      assertThatCode(() -> negotiator.negotiate(Collections.singletonList(mediaType)))
          .doesNotThrowAnyException();
    }

    @Test
    void returnsWildcardType() throws NotAcceptableException {
      MediaType mediaType = MediaType.APPLICATION_JSON;

      ContentTypeNegotiator negotiator = new ContentTypeNegotiator();

      MediaType actual = negotiator.negotiate(Collections.singletonList(mediaType));

      assertThat(actual).isEqualTo(MediaType.WILDCARD);
    }
  }

  @Nested
  class ConstructorValidation {

    @Test
    void throwsIllegalArgumentExceptionWhenEmptySupportedValues() {
      assertThatThrownBy(() -> new ContentTypeNegotiator(Collections.emptyList()))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage(
              "Unable to construct a ContentTypeNegotiator without supported MediaTypes. If you want to allow any MediaType, you need to use the default constructor");
    }
  }

  @ParameterizedTest
  @NullAndEmptySource
  void nullOrEmptyMediaTypesThrowNotAcceptableException(List<MediaType> mediaTypes) {
    assertThatThrownBy(() -> negotiator.negotiate(mediaTypes))
        .isInstanceOf(NotAcceptableException.class)
        .hasMessage("The requested media types cannot be supported by this server");
  }

  @Test
  void throwsNotAcceptableExceptionWhenNotSupported() {
    assertThatThrownBy(
            () -> negotiator.negotiate(Collections.singletonList(MediaType.MULTIPART_FORM_DATA)))
        .isInstanceOf(NotAcceptableException.class);
  }

  @Test
  void throwsNotAcceptableExceptionWhenNotSpecificEnough() {
    negotiator = new ContentTypeNegotiator(asList(MediaType.APPLICATION_JSON, JSON_API));

    assertThatThrownBy(
            () ->
                negotiator.negotiate(Collections.singletonList(MediaType.valueOf("application/*"))))
        .isInstanceOf(NotAcceptableException.class);
  }

  @Test
  void returnsMediaTypeThatIsSupported() throws NotAcceptableException {
    MediaType actual = negotiator.negotiate(Collections.singletonList(MediaType.APPLICATION_JSON));

    assertThat(actual).isEqualTo(MediaType.APPLICATION_JSON);
  }

  @Test
  void performsQualityBasedNegotiation() throws NotAcceptableException {
    negotiator = new ContentTypeNegotiator(asList(MediaType.APPLICATION_JSON, JSON_API));

    MediaType json = new MediaType("application", "json", Collections.singletonMap("q", "0.3"));

    MediaType actual = negotiator.negotiate(asList(json, JSON_API));

    assertThat(actual).isEqualTo(JSON_API);
  }

  @Test
  void performsQualityBasedNegotiation1() throws NotAcceptableException {
    MediaType json = new MediaType("application", "json");

    MediaType actual = negotiator.negotiate(asList(json));

    assertThat(actual).isEqualTo(MediaType.APPLICATION_JSON);
  }

  @Test
  void performsSpecificityBasedNegotiation() throws NotAcceptableException {
    MediaType json = new MediaType("application", "*");

    MediaType actual = negotiator.negotiate(Collections.singletonList(json));

    assertThat(actual).isEqualTo(MediaType.APPLICATION_JSON);
  }

  @Test
  void handlesSuffixNegotiation() throws NotAcceptableException {
    negotiator = new ContentTypeNegotiator(asList(MediaType.APPLICATION_JSON));

    MediaType json = new MediaType("application", "*+xml");

    assertThatThrownBy(() -> negotiator.negotiate(asList(json)));
  }

  @Nested
  class RfcExample {

    private final MediaType plain = MediaType.valueOf("text/plain; q=0.5");
    private final MediaType html = MediaType.valueOf("text/html");
    private final MediaType xdvi = MediaType.valueOf("text/x-dvi; q=0.8");
    private final MediaType xc = MediaType.valueOf("text/x-c");
    private final List<MediaType> quality1Options = Arrays.asList(html, xc);

    @BeforeEach
    void setup() {
      negotiator = new ContentTypeNegotiator(asList(plain, html, xdvi, xc));
    }

    @ParameterizedTest
    @ValueSource(strings = {"text/html", "text/x-c"})
    void quality1IsOfEqualChanceOfNegotiation(String mediaType) throws NotAcceptableException {
      MediaType actual =
          negotiator.negotiate(Collections.singletonList(MediaType.valueOf(mediaType)));

      assertThat(quality1Options).contains(actual);
    }

    @Test
    void quality1IsSelected() throws NotAcceptableException {
      MediaType actual = negotiator.negotiate(asList(plain, html, xdvi, xc));

      assertThat(quality1Options).contains(actual);
    }

    @Test
    void xdviIsIgnoredIfQuality1Choices() throws NotAcceptableException {
      List<MediaType> options = Arrays.asList(html, xc);

      MediaType actual = negotiator.negotiate(asList(html, xdvi));

      assertThat(options).contains(actual);
    }

    @Test
    void xdviIsResolvedIfNoQuality1Choices() throws NotAcceptableException {
      MediaType actual = negotiator.negotiate(Collections.singletonList(xdvi));

      assertThat(actual).isEqualTo(MediaType.valueOf("text/x-dvi"));
    }

    @Test
    void plainIsResolvedLast() throws NotAcceptableException {
      MediaType actual = negotiator.negotiate(Collections.singletonList(plain));

      assertThat(actual).isEqualTo(MediaType.valueOf("text/plain"));
    }
  }

  @Nested
  class ExternalTestCases {
    private final AcceptHeaderParser acceptHeaderParser = new AcceptHeaderParser();
    private WhitespaceIgnoringComparator whitespaceIgnoringComparator =
        new WhitespaceIgnoringComparator();

    @ParameterizedTest(name = "{0}")
    @ArgumentsSource(ExternalTestCaseArgumentProvider.class)
    void externalTestCasesAreValid(
        String name, List<String> supported, String acceptHeader, List<String> negotiated)
        throws NotAcceptableException {
      negotiator = new ContentTypeNegotiator(parse(supported));

      List<MediaType> parsedAcceptHeader = acceptHeaderParser.parse(acceptHeader);
      MediaType actual;

      switch (negotiated.size()) {
        case 0:
          assertThatThrownBy(() -> negotiator.negotiate(parsedAcceptHeader))
              .isInstanceOf(NotAcceptableException.class);
          break;
        case 1:
          actual = negotiator.negotiate(parsedAcceptHeader);

          assertThat(actual).isNotNull();
          assertThat(actual.toString()).isEqualToIgnoringWhitespace(negotiated.get(0));
          break;
        default:
          actual = negotiator.negotiate(parsedAcceptHeader);

          assertThat(actual).isNotNull();
          assertThat(negotiated)
              .usingElementComparator(whitespaceIgnoringComparator)
              .contains(actual.toString());
          break;
      }
    }

    private List<MediaType> parse(List<String> mediaTypes) {
      return mediaTypes.stream().map(MediaType::valueOf).collect(Collectors.toList());
    }
  }

  private List<MediaType> asList(MediaType... mediaTypes) {
    return Arrays.asList(mediaTypes);
  }
}
